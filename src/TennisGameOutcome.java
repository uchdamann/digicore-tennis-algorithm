public class TennisGameOutcome {
	
	public static void main(String[] args) {
		int[] tomScores = {3, 6, 7, 9, 3, 2};
		int[] jackScores = {3, 2, 3, 8, 3, 9};
		int [] gameOutcome = getGameWinner(tomScores, jackScores);
		
		System.out.println("The game ends with the scoreline as follows:");
		System.out.println("Tom's Score - "+gameOutcome[0]+"\nJack's Score - "+gameOutcome[1]);
	}
	
	public static int[] getGameWinner(int[] tomScores, int[] jackScores) {
		int[] wins = new int[2];
		int tomWins = 0;
		int jackWins = 0;
		
		for (int i = 0; i < tomScores.length; i++) {
			if (tomScores[i] > jackScores[i]) {
				tomWins++;
			}
			else if (tomScores[i] < jackScores[i]) {
				jackWins++;
			}
		}
		wins[0] = tomWins;
		wins[1] = jackWins;
		
		return wins;
	}

}
